package db;

import java.util.Random;

import com.adamantdreamer.quorm.define.Auto;
import com.adamantdreamer.quorm.define.Foreign;
import com.adamantdreamer.quorm.define.Id;
import com.adamantdreamer.quorm.define.Schema;
import com.adamantdreamer.quorm.define.Size;

@Schema("_test")
public class Child {

	@Id @Auto
	private int id;
	
	@Foreign
	private Parent parent;
	
	@Size(16)
	private String string;
	
	
	
	public Child(Parent parent) {
		this.parent = parent;
		
		StringBuilder randomBuilder = new StringBuilder();
		Random rnd = new Random();
		for (int i = 0; i < 16; i++) {
			randomBuilder.append(Character.toChars(rnd.nextInt(94)+33));
		}
		string = randomBuilder.toString();
	}
		
}
