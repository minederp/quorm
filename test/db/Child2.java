package db;

import java.util.Random;

import com.adamantdreamer.quorm.define.Auto;
import com.adamantdreamer.quorm.define.DefineIndex;
import com.adamantdreamer.quorm.define.Id;
import com.adamantdreamer.quorm.define.Index;
import com.adamantdreamer.quorm.define.Schema;
import com.adamantdreamer.quorm.define.Size;
import com.adamantdreamer.quorm.index.IndexType;

@Schema("_test")
@DefineIndex(value="personIndex", type=IndexType.FOREIGN_KEY, table="parent")
public class Child2 {
	@Id @Auto
	private int id;
	
	@Index(value="personIndex", related="id")
	private int personId;
	
	@Size(16)
	private String string;
	
	
	
	public Child2(Parent person) {
		this.personId = person.getId();
		
		StringBuilder randomBuilder = new StringBuilder();
		Random rnd = new Random();
		for (int i = 0; i < 16; i++) {
			randomBuilder.append(Character.toChars(rnd.nextInt(94)+33));
		}
		string = randomBuilder.toString();
	}
		
}
