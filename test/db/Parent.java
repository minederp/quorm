package db;


import com.adamantdreamer.quorm.define.Auto;
import com.adamantdreamer.quorm.define.DefineIndex;
import com.adamantdreamer.quorm.define.DefineIndices;
import com.adamantdreamer.quorm.define.Id;
import com.adamantdreamer.quorm.define.Index;
import com.adamantdreamer.quorm.define.Schema;
import com.adamantdreamer.quorm.define.Size;
import com.adamantdreamer.quorm.index.IndexType;

@Schema("_test")
@DefineIndices({
	@DefineIndex(value="uniqueIndex", type=IndexType.UNIQUE_KEY),
	@DefineIndex(value="regularIndex", type=IndexType.INDEX)
})
public class Parent {
	
	@Id @Auto
	private int id;
	
	@Id
	private int id2;
	
	@Size(15)
	private String name;
	
	@Index(value="regularIndex", order=0) @Size(3)
	private String test;
	
	public Parent() {}
	
	public Parent(int id, int id2) {
		this.id = id;
		this.id2 = id2;
	}	
	public Parent(int id2) {
		//this.id = id;
		this.id2 = id2;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public int getId() {
		return id;
	}
	
}
