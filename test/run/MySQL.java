package run;


import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.adamantdreamer.quorm.core.DAO;
import com.adamantdreamer.quorm.core.Link;
import com.adamantdreamer.quorm.core.Schema;
import com.adamantdreamer.quorm.elements.TableManager;
import com.adamantdreamer.quorm.link.mysql.MysqlLink;
import com.adamantdreamer.quorm.query.QueryResult;

import db.Child;
import db.Child2;
import db.Parent;

public class MySQL {
	
	static private DAO dao;

	@BeforeClass
	static public void start() throws Exception {
		Link link = new MysqlLink("localhost","3306","root","pass");
    	link.connect();
    	dao = link.getDAO();
    	dao.getLink().getStatement().executeUpdate("DROP SCHEMA IF EXISTS _test");
    	
    	//Schema schema = new Schema("_test", Parent.class, Child.class, Child2.class);
    	//dao.build(schema);
    	dao.build(Parent.class, Child.class, Child2.class);
    }
	
	@AfterClass
	static public void stop() {
    	dao.getLink().close();
    	TableManager.clear();
	}	
	
	@Test
	public void crud() throws SQLException {
		ResultSet rs;
		
		Parent p = new Parent(5);
		p.setName("John");
		

		
		// check create
		dao.create(p);
		rs = dao.getLink().getStatement().executeQuery("SELECT Name FROM _test.parent WHERE Id=" + p.getId());
		rs.next();
		Assert.assertEquals("Create failed", "John", rs.getString("Name"));
		rs.close();

		// check cache
		p.setName(null);
		dao.read(p);
		Assert.assertEquals("Cache read failed", null, p.getName());
		
		//  clear cache and check read
		dao.get(Parent.class).getCache().clear();
		int id = p.getId();
		p = dao.read(new Parent(id, 5));
		Assert.assertEquals("DB read failed", "John", p.getName());

		// check update
		p.setName("Doe");
		dao.update(p);
		rs = dao.getLink().getStatement().executeQuery("SELECT Name FROM _test.parent WHERE Id=" + p.getId());
		rs.next();
		Assert.assertEquals("Create failed", "Doe", rs.getString("Name"));
		rs.close();
		
		// check delete
		dao.delete(p);
		rs = dao.getLink().getStatement().executeQuery("SELECT Count(*) FROM _test.parent WHERE Id=" + p.getId());
		rs.next();
		Assert.assertEquals("deleted failed", 0, rs.getInt(1));
		rs.close();
		
	}
	
	@Test 
	public void query() throws SQLException {
		dao.getLink().getStatement().executeUpdate("INSERT INTO _test.parent (`id2`,`name`,`test`) VALUES(5,\"Jane\", \"t\")");
		QueryResult<Parent> result;

		// with parameters
		result = dao.read(Parent.class).where("NAME LIKE ?").read("Jane");
		Parent p1 = result.first();
		Assert.assertEquals("query with parameter failed", "Jane", p1.getName());

		// without parameters
		result = dao.read(Parent.class).where("NAME LIKE \"Jane\"").read();
		Parent p2 = result.first();
		Assert.assertEquals("query without parameter failed", "Jane", p2.getName());

		// Cache
		Assert.assertEquals("Query cache failed", p1, p2);
	}

	
	
	

}
