package run;


import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.adamantdreamer.quorm.core.Schema;
import com.adamantdreamer.quorm.elements.TableStructure;
import com.adamantdreamer.quorm.elements.TableManager;

import db.Child;
import db.Parent;

public class Structure {
	static private Schema schema;
	
	@BeforeClass
	static public void begin() {
		schema = new Schema("schema" , Parent.class, Child.class);
	}
	
	@AfterClass
	static public void end() {
		TableManager.clear();
	}

	@Test
	public void Schema() {
		Assert.assertEquals("schema", schema.getName());
		Assert.assertEquals(2, schema.getTables().size());
		Assert.assertEquals(Parent.class, schema.getTables().get(0));
		Assert.assertEquals(Child.class, schema.getTables().get(1));
	}
	
	@Test
	public void Table() {
		TableStructure<Parent> structure = TableManager.get(Parent.class);
		Assert.assertEquals(Parent.class.getDeclaredFields().length, structure.getColumns().size());
	}
}
