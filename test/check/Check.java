package check;

import java.lang.reflect.Field;
import java.util.List;

import com.adamantdreamer.quorm.core.Schema;
import com.adamantdreamer.quorm.elements.Column;
import com.adamantdreamer.quorm.elements.TableStructure;
import com.adamantdreamer.quorm.elements.TableManager;

public class Check {

	public void table(Class<?> table) {
		
		//Build table if needed.
		if (TableManager.get(table) == null) {
			@SuppressWarnings("unused")
			Schema testSchema = new Schema("Test",table);
		}
		
		TableStructure<?> data = TableManager.get(table);
		System.out.println("Describing Table " + data.getName());
		List<Column> fields = data.getColumns();
		for (Column column: fields) {
			System.out.println("  " + column.getName() + " " + column.getType() + "(" + column.getSize() + ")");
		}
	}
	
	public void instance(Object o) throws IllegalArgumentException, IllegalAccessException {
		Class<?> clazz = o.getClass();
		System.out.println("Describing Instance " + o.getClass().getName());
		while (!(clazz instanceof Object)) {
			for (Field field: clazz.getDeclaredFields()) {
				field.setAccessible(true);
				System.out.println("  " + field.getName() + ": " + field.get(o));
			}
			clazz  = o.getClass().getSuperclass();
		}
	}
	
}
