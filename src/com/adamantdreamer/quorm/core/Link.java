package com.adamantdreamer.quorm.core;

import java.sql.Connection;
import java.sql.Statement;

public interface Link {

	public void connect() throws Exception;
	public void close();
	public Connection getConnection();
	public Statement getStatement();
	
	//public void build(Schema... schemas) throws SQLException, ColumnMismatchException;
	public DAO getDAO();
	
}
