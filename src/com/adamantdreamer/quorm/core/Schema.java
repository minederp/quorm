package com.adamantdreamer.quorm.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.adamantdreamer.quorm.define.Name;
import com.adamantdreamer.quorm.elements.TableManager;

public class Schema {
	static private Map<String,Schema> schemaMap = new HashMap<>();

	/**
	 * Gets schema by name.  If the schema does not exist, it 
	 * will be created.
	 */
	static public Schema get(String name) {
		if (schemaMap.containsKey(name)) {
			return schemaMap.get(name);
		}
		return new Schema(name);
	}
	
	static public void remove(String name) {
		schemaMap.remove(name);
	}
	
	public List<Class<?>> tableList = new ArrayList<>();
	
	private String name;
		
	protected Schema(Class<?>... tables) {
		this.name = (getClass().isAnnotationPresent(Name.class)
				? getClass().getAnnotation(Name.class).value()
				: getClass().getSimpleName().toLowerCase());
		addTables(tables);
	}
	
	/**
	 * 
	 * @param name
	 * @throws IllegalArgumentException If a schema with the same name already exists.
	 */
	public Schema(String name) {
		if (schemaMap.containsKey(name)) {
			throw new IllegalArgumentException("Schema already exists.");
		}
		this.name = name;
		schemaMap.put(name, this);
	}
	
	public Schema(String name, Class<?>... tables) {
		this(name);
		addTables(tables);
	}

	public void addTables(Class<?>... tables) {
		for (Class<?> table: tables) {
			TableManager.add(this, table);
			tableList.add(table);
		}		
	}
	
	public String getName() {
		return name;
	}
	
	public List<Class<?>> getTables() {
		return tableList;
	}

}
