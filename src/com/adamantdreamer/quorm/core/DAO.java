package com.adamantdreamer.quorm.core;

import java.sql.SQLException;

import com.adamantdreamer.quorm.exception.ColumnMismatchException;
import com.adamantdreamer.quorm.query.Query;

public interface DAO {

	public Link getLink();
	public <T> TableDAO<T> get(Class<T> table);

	public void build(Schema... schemas) throws SQLException, ColumnMismatchException;
	public void build(Class<?>... tables) throws SQLException, ColumnMismatchException;

// Record //
	
	public <T> T create(T record) throws SQLException;
	public <T> T read(T record) throws SQLException;
	public <T> boolean update(T record) throws SQLException;
	public <T> boolean delete(T record) throws SQLException;
	
// Query //
	
	public <T> Query<T> query(String sqlString);
	public <T> Query<T> create(Class<T> table);
	public <T> Query<T> read(Class<T> table);
	public <T> Query<T> update(Class<T> table);
	public <T> Query<T> delete(Class<T> table);

}
