package com.adamantdreamer.quorm.core;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import com.adamantdreamer.quorm.elements.TableStructure;
import com.adamantdreamer.quorm.index.RecordHashKey;
import com.adamantdreamer.quorm.query.QueryType;
import com.adamantdreamer.quorm.util.cache.Cache;


public interface TableDAO<T> {

	public TableStructure<T> getStructure();

	/**
	 * Creates a new record based on an existing object. If the object
	 * has an auto incremented value, it will be updated.
	 * @param record  
	 * @return
	 * @throws SQLException 
	 */
	public T create(T record) throws SQLException;

	/**
	 * Reads a record with key values matching the object.
	 * 
	 * @param record
	 * @return
	 * @throws SQLException
	 */
	public T read(T record) throws SQLException;

	public T read(ResultSet rs) throws SQLException;
	
	/**
	 * Updates the database.
	 * 
	 * @param object
	 * @return
	 * @throws SQLException 
	 */
	public boolean update(T record) throws SQLException;
	
	/**
	 * Deletes a record.
	 * 
	 * @param object Object that matches the primary keys of the record.
	 * @return True if the object existed in the database, false if it was not found.
	 * @throws SQLException 
	 */
	public boolean delete(T record) throws SQLException;

	public Map<QueryType, String> getQueries();
	
	public Cache<RecordHashKey, T> getCache();

}
