package com.adamantdreamer.quorm.exception;

import com.adamantdreamer.quorm.elements.Column;

/**
 * Thrown when a column structure  
 */
public class ColumnMismatchException extends Exception {
	private static final long serialVersionUID = 8501711595659524172L;

	/**
	 * Caused when the structure of an existing database column doesn't match the
	 * structure of the java object and definitions.
	 * 
	 * @param databaseColumn
	 * @param objectColumn
	 */
	public ColumnMismatchException(Column databaseColumn, Column objectColumn) {
		super("Column structure on " + databaseColumn.getTable().getName() +  "." + databaseColumn.getName() 
				+ "does not match existing table.");
	}

}
