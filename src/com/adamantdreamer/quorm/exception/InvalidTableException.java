package com.adamantdreamer.quorm.exception;

public class InvalidTableException extends RuntimeException {
	private static final long serialVersionUID = -6071200872583659466L;

	public InvalidTableException(String cause) {
		super(cause);
	}
	
}
