package com.adamantdreamer.quorm.exception;

/**
 * Thrown when the reflection system catches one of the following issues:<br>
 * <ul><li>InstantiationException</li>
 *   <li>IllegalArgumentException</li>
 *   <li>IllegalAccessException</li>
 *   </ul>
 */
public class DataAccessException extends RuntimeException {
	private static final long serialVersionUID = -6669701700637752816L;
	
	public DataAccessException(String reason) {
		super(reason);
	}

	public DataAccessException(String reason, Exception ex) {
		super(reason, ex);
	}
	
}
