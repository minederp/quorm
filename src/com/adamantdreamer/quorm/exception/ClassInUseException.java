package com.adamantdreamer.quorm.exception;

import com.adamantdreamer.quorm.core.Schema;

/**
 * Thrown when a class already is in use by another table.
 */
public class ClassInUseException extends RuntimeException {
	private static final long serialVersionUID = -6669701700637752816L;

	public ClassInUseException(Class<?> table, Schema usedBy, Schema addedBy) {
		super("Can not add " + table.getName() + " to schema " + addedBy.getName() + ", Class already in use by schema " + usedBy.getName() + ".");
	}

}
