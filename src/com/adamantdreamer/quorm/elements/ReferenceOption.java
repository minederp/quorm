package com.adamantdreamer.quorm.elements;

public enum ReferenceOption {

	/**
	 * The update/delete action will be prevented, causing an SQL error to occur at the beginning of the
	 * transaction.
	 */
	RESTRICT("RESTRICT"),
	/**
	 * If the key value is updated or the record is deleted, the changes will occur within the child
	 * table as well.
	 */
	CASCADE("CASCADE"),
	/**
	 * Any referenced records will be removed if the parent record is deleted or has a related value updated. 
	 */
	NULL("SET NULL"),
	/**
	 * The update/delete action will be prevented, causing an SQL error to occur at the end of the
	 * transaction.
	 */
	DEFAULT("NO ACTION");
	
	private String sqlString;
	
	private ReferenceOption(String sqlString) {
		this.sqlString = sqlString;
	}
	
	public String getString() {
		return sqlString;
	}
	
	
}
