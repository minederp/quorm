package com.adamantdreamer.quorm.elements;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.adamantdreamer.quorm.core.Schema;
import com.adamantdreamer.quorm.define.Auto;
import com.adamantdreamer.quorm.define.DefineIndex;
import com.adamantdreamer.quorm.define.DefineIndices;
import com.adamantdreamer.quorm.define.Foreign;
import com.adamantdreamer.quorm.define.Id;
import com.adamantdreamer.quorm.define.Ignore;
import com.adamantdreamer.quorm.define.Index;
import com.adamantdreamer.quorm.define.Indices;
import com.adamantdreamer.quorm.define.Name;
import com.adamantdreamer.quorm.exception.InvalidTableException;
import com.adamantdreamer.quorm.index.ForeignIndex;
import com.adamantdreamer.quorm.index.BaseIndex;
import com.adamantdreamer.quorm.index.IndexType;
import com.adamantdreamer.quorm.index.LocalIndex;

public class TableStructure<T> {

	private Schema schema;

	private String name;
	//private Map<String, IndexStructure> indexMap = new HashMap<>();
	private Map<String, BaseIndex> indexMap = new HashMap<>();
	private Map<String, Column> columnMap = new LinkedHashMap<>();
	private Map<Column, TableStructure<?>> foreignMap = null;

	private Class<T> table;

	private Column autoColumn = null;
	private boolean hasTimestamp = false;
	
	public TableStructure(Class<T> table) {
		this(Schema.get(table.getAnnotation(com.adamantdreamer.quorm.define.Schema.class).value()), table);
	}

	public TableStructure(Schema schema, Class<T> table) {
		this.schema = schema;
		this.table = table;
		name = (table.isAnnotationPresent(Name.class) 
				? table.getAnnotation(Name.class).value() 
				: table.getSimpleName().toLowerCase());
		
		// Build Indices
		indexMap.put("", new LocalIndex("", IndexType.PRIMARY_KEY));
		if (table.isAnnotationPresent(DefineIndex.class)) {
			addIndex(table.getAnnotation(DefineIndex.class));
		} else if (table.isAnnotationPresent(DefineIndices.class)) {
			for (DefineIndex defineIndex: table.getAnnotation(DefineIndices.class).value()) {
				addIndex(defineIndex);
			}
		}
		
		// Gather column Structure
		Class<?> buildFromClass = table;
		while (buildFromClass != java.lang.Object.class) {
			addColumns(buildFromClass);
			buildFromClass = buildFromClass.getSuperclass();
		}		
	}
	
// Get //
	
	public Class<T> getTable() {
		return table;
	}

	public String getName() {
		return name;
	}	
	
	public Schema getSchema() {
		return schema;
	}

	// Immutable
	public List<Column> getColumns() {
		return new ArrayList<Column>(columnMap.values());	
	}
	
	public List<Column> get(String... columnNames) {
		List<Column> result = new ArrayList<Column>();
		Column column;
		for (String columnName: columnNames) {
			column = columnMap.get(columnName);
			result.add(column);
		}
		return result;
	}

	public Collection<BaseIndex> getIndices() {
		return indexMap.values();
	}
	
	public LocalIndex getPrimaryKey() {
		return (LocalIndex)indexMap.get("");
	}
	
	public boolean hasAutoColumn() {
		return (autoColumn != null);
	}	

	public Column getAutoColumn() {
		return autoColumn;
	}

	
	public Map<Column, TableStructure<?>> getForeignMap() {
		return foreignMap;
	}

	public boolean hasForeignColumns() {
		return (foreignMap != null);
	}
	
	public boolean hasGeneratedColumns() {
		return hasTimestamp;
	}	
	
// Main //
	
	private void addIndex(DefineIndex defineIndex) {
		indexMap.put(defineIndex.value(),
				(defineIndex.type() != IndexType.FOREIGN_KEY) 
				? new LocalIndex(defineIndex.value(), defineIndex.type())
				: new ForeignIndex(defineIndex.value(),			
						defineIndex.table(),defineIndex.onUpdate(), defineIndex.onDelete())
				);
	}

	private void addColumns(Class<?> table) {
		for (java.lang.reflect.Field reflect: table.getDeclaredFields()) {
			if (!reflect.isAnnotationPresent(Ignore.class)) {
				addColumn(reflect);
			}
		}
	}
	
	private void addColumn(Field reflect) {
		Column column = new Column(this, reflect);
		
		if (reflect.isAnnotationPresent(Foreign.class)) {
			Foreign foreign = reflect.getAnnotation(Foreign.class);
			
			TableStructure<?> foreignTable = TableManager.get(reflect.getType());
			if (foreignTable == null) {
				throw new InvalidTableException("TableStructure does not exist for " + reflect.getType());
			}
			
			// generate Foreign Key index
			ForeignIndex index = new ForeignIndex(
					reflect.getName() + "_index" ,
					foreignTable.getSchema().getName() + "." + foreignTable.getName(),
					foreign.onUpdate(), foreign.onDelete());
			indexMap.put(index.getName(), index);

			if (foreignMap == null) {
				foreignMap = new LinkedHashMap<>();
			}
			for (Column relatedColumn: foreignTable.getPrimaryKey().getColumns()) {
				Column foreignColumn = new ForeignColumn(this, reflect, relatedColumn);
				columnMap.put(foreignColumn.getName(), foreignColumn);
				if (reflect.isAnnotationPresent(Id.class)) {
					((LocalIndex)getPrimaryKey()).addColumn(foreignColumn);
				}
				index.addColumn(foreignColumn, relatedColumn.getName());
			}
			foreignMap.put(column, foreignTable);
			

			return;
		}
		
		columnMap.put(column.getName(), column);
		
		if (reflect.isAnnotationPresent(Id.class)) {
			getPrimaryKey().addColumn(column);
		} else if (reflect.isAnnotationPresent(Index.class)) {
			addColumnToIndex(column, reflect.getAnnotation(com.adamantdreamer.quorm.define.Index.class));
		} else if (reflect.isAnnotationPresent(Indices.class)) {
			for (com.adamantdreamer.quorm.define.Index index: reflect.getAnnotation(Indices.class).value()) {
				addColumnToIndex(column, index);
			}
		}
		
		if (reflect.isAnnotationPresent(Auto.class)) {
			autoColumn = column;
		}
		
		if (column.getType() == DataType.TIMESTAMP) {
			hasTimestamp = true;
		}		
	}

	private void addColumnToIndex(Column column, Index index) {
		if (! indexMap.containsKey(index.value())) {
			throw new IllegalArgumentException("Index '" + index.value() + "' not found.");	
		}
		
		if (index.related().isEmpty()) {
			((LocalIndex)indexMap.get(index.value())).setColumn(index.order(), column);
		} else {
			((ForeignIndex)indexMap.get(index.value())).setColumn(index.order(), column, index.related());
		}
		
	}

}
