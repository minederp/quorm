package com.adamantdreamer.quorm.elements;

import java.util.HashMap;
import java.util.Map;

import com.adamantdreamer.quorm.core.Schema;
import com.adamantdreamer.quorm.exception.ClassInUseException;

public class TableManager {

	static private Map<Class<?>, TableStructure<?>> tableMap = new HashMap<>();
	
	/**
	 * Generates and stores the the Table Structure.   
	 * 
	 * @throws ClassInUseException Caused when the table already exists in another schema.
	 */
	static public void add(Schema intoSchema, Class<?> table) throws ClassInUseException {
		if (tableMap.containsKey(table)) {
			if (tableMap.get(table).getSchema() != intoSchema) {
				throw new ClassInUseException(table, intoSchema, tableMap.get(table).getSchema());
			}
		}
		tableMap.put(table, new TableStructure<>(intoSchema, table));
	}

	/**
	 * Generates and stores the the Table Structure, using the schema defined within the table.   
	 * 
	 * @param table
	 * @return Schema used by the table.
	 */
	public static Schema add(Class<?> table) {
		TableStructure<?> structure = new TableStructure<>(table);
		tableMap.put(table, structure);
		return structure.getSchema();
	}

	
	/**
	 * Returns the Table Structure for a class, or null if one has not been created. 
	 */
	@SuppressWarnings("unchecked")
	static public <T> TableStructure<T> get(Class<T> table) {
		return (TableStructure<T>) tableMap.get(table);
	}
	
	static public boolean remove(Class<?> table) {
		return (tableMap.remove(table) != null);
	}
	
	static public void clear() {
		tableMap.clear();
	}


}
