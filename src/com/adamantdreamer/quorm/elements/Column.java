package com.adamantdreamer.quorm.elements;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.adamantdreamer.quorm.define.Auto;
import com.adamantdreamer.quorm.define.Default;
import com.adamantdreamer.quorm.define.Name;
import com.adamantdreamer.quorm.define.NotNull;
import com.adamantdreamer.quorm.define.Size;
import com.adamantdreamer.quorm.define.Type;
import com.adamantdreamer.quorm.define.Var;
import com.adamantdreamer.quorm.exception.DataAccessException;

public class Column {
	
	private Field reflection;
	
	private TableStructure<?> table;

	private String name;
	private DataType dataType;
	private int size = 0;
	private boolean notNull;
	private String defaultValue;
	private boolean autoIncrement;

	public Column(TableStructure<?> table, Field reflection) {
		this.table = table;
		this.reflection = reflection;
		
		reflection.setAccessible(true);
		 
		// Name
		Name nameAnnotation = reflection.getAnnotation(Name.class);
		if (nameAnnotation == null) { 
			name = reflection.getName().toLowerCase();
		} else {
			name = nameAnnotation.value().toLowerCase();
		}
		
		// Size
		if (reflection.isAnnotationPresent(Size.class)) {
			size = reflection.getAnnotation(Size.class).value();
		}
		
		// Data Type
		Type define = reflection.getAnnotation(Type.class);
		if (define != null) {
			dataType = define.value();
		} else {
			dataType = DataType.getDefault(reflection.getType().getSimpleName());
			if (dataType == DataType.TEXT && size < 256) {
				dataType = (reflection.isAnnotationPresent(Var.class) ? DataType.VARCHAR : DataType.CHAR);
			}
		}
		
		// Nulls allowed unless type is primitive or NotNull is used.
		notNull = (reflection.getType().isPrimitive() || reflection.isAnnotationPresent(NotNull.class));
		
		// Default Value
		Default defaultAnno = reflection.getAnnotation(Default.class);
		defaultValue = (defaultAnno == null ? null : defaultAnno.value());
		
		//Auto-Increment
		autoIncrement = (reflection.isAnnotationPresent(Auto.class));
	}
	
	public Column(TableStructure<?> table, Field reflection, String name, DataType type, Integer size, boolean notNull, String defaultValue, Boolean autoIncrement) {
		this.table = table;
		this.reflection = reflection;
		
		this.name = name;
		this.dataType = type;
		if (size != null) {
			this.size = size;
		} else {
			this.size = -1;
		}
		this.notNull = notNull;
		this.defaultValue = defaultValue;
		this.autoIncrement = autoIncrement;
	}		

// Reflection //
	
	public Object get(Object record) {
		if (record == null) {
			return null;
		}
		
		try {
			return reflection.get(record);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw new DataAccessException("Unable to get field value.", e);
		} 
	}
	
	
	public void set(Object record, Object value) {

		try {
			reflection.set(record, value);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw new DataAccessException("Unable to set field value.\n"
					+ "Record: " + record + "\n"
					+ "Field: " + getName() + " " + getType() + "\n" 
					+ "Value:" + value, e);
		}

	}


	public void copy(ResultSet fromSet, Object toRecord) throws SQLException, ClassNotFoundException, IOException {
		//String name = structure.getTable().getName() + "." + structure.getName();
		switch (getType()) {
		case BLOB:
		    byte[] buf = fromSet.getBytes(getName());
		    if (buf != null) {
		      set(toRecord, (new ObjectInputStream(new ByteArrayInputStream(buf))).readObject());
		    }
		    break;
		case SMALLINT:
			set(toRecord, fromSet.getShort(getName()));
			break;
		case TINYINT:
			set(toRecord, fromSet.getByte(getName()));
			break;
		default:
			set(toRecord, fromSet.getObject(getName()));
			break;
		}			
	}
	
	/**
	 * Gets the related column, putting it into a prepared statement. Used by
	 * TAO's, as the generic PreparedStatement.setObject() does not work
	 * with some java object types.
	 */  
	public void copy(Object fromRecord, PreparedStatement toStatement, int usingIndex) throws SQLException {
		toStatement.setObject(usingIndex, get(fromRecord));
	}

	public boolean isForeign() {
		return false;
	}

// Structure //
	
	public TableStructure<?> getTable() {
		return table;
	}
	
	public String getName() {
		return name;
	}	
	
	public DataType getType() {
		return dataType;
	}

	public int getSize() {
		return size;
	}

	public boolean isNotNull() {
		return notNull;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public boolean isAutoIncrement() {
		return autoIncrement;
	}
	

}
