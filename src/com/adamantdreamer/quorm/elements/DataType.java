package com.adamantdreamer.quorm.elements;


/**
 * QuORM uses the standard SQL Data Types:</p>
 *  
 * <ul>
 * <li><b>Text/Data:</b> CHAR, VARCHAR, TEXT, BLOB</li>
 * <li><b>Integer:</b> BOOLEAN, TINYINT, SMALLINT, MEDIUMINT, INT, BIGINT</li>
 * <li><b>Numeric:</b> FLOAT, DOUBLE, DECIMAL</li> 
 * <li><b>Time:</b> DATE, DATETIME, TIMESTAMP</li>
 * </ul>
 *
 */
public enum DataType {
	BOOLEAN,
	TINYINT,
	SMALLINT,
	MEDIUMINT,
	INT,
	BIGINT,
	
	FLOAT,
	DOUBLE,
	DECIMAL,

	CHAR,
	VARCHAR,
	TEXT,
	
	BLOB,
	
	DATE,
	DATETIME,
	TIMESTAMP;
	
	/**
	 * Converts java primitives, wrappers, and other objects into their respective SQL data types.
	 * 
	 * @return null if there is no default data type for the field.
	 */
	private enum DefaultConverter {
		BOOLEAN(DataType.BOOLEAN),
		
		BYTE(DataType.TINYINT),
		SHORT(DataType.SMALLINT),
		INT(DataType.INT),
		INTEGER(DataType.INT),
		LONG(DataType.BIGINT),
		
		FLOAT(DataType.FLOAT),
		DOUBLE(DataType.DOUBLE),
		STRING(DataType.TEXT),
		
		DATE(DataType.DATE),
		DATETIME(DataType.DATETIME),
		TIMESTAMP(DataType.TIMESTAMP);
		
		private DataType dataType;

		private DefaultConverter(DataType type) { 
			this.dataType = type; 
		};
		
		public DataType getType() {
			return dataType;
		}
		
		public static DataType get(String objectName) {
			try {
				return DefaultConverter.valueOf(objectName).getType();
			} catch (IllegalArgumentException ex) {
				return DataType.BLOB;
			}
		}
			
	}
	
	public static DataType get(String value) {
		return valueOf(value.toUpperCase());
	}

	/**
	 * Returns a data type based on the Object's name.  If the object does not have a known
	 * type, it defaults to BLOB.
	 * 
	 * @param objectName String representing the Object's name.
	 */
	
	public static DataType getDefault(String objectName) {
		return DefaultConverter.get(objectName.toUpperCase());
	}
	

}
