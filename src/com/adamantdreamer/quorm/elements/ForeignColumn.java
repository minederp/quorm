package com.adamantdreamer.quorm.elements;

import com.adamantdreamer.quorm.exception.DataAccessException;

public class ForeignColumn extends Column {
	
	private Column relatedColumn;

	public ForeignColumn(TableStructure<?> table, java.lang.reflect.Field reflect, Column foreignColumn) {
		super(table, reflect, reflect.getName() + "_" + foreignColumn.getName(), 
				foreignColumn.getType(), foreignColumn.getSize(), false, null, false);
		this.relatedColumn = foreignColumn;
	}
	
	public Column getRelatedColumn() {
		return relatedColumn;
	}

	@Override
	public Object get(Object record) {
		Object foreignRecord = super.get(record);
		if (foreignRecord == null) {
			return null;
		}
		return relatedColumn.get(foreignRecord);
	}
	
	@Override
	public void set(Object record, Object value) {
		if (value == null) {
			super.set(record, value);
			return;
		}
		
		Object foreignRecord = super.get(record);
		if (foreignRecord == null) {
			try {
				foreignRecord = relatedColumn.getTable().getTable().newInstance();
				super.set(record, foreignRecord);
			} catch (InstantiationException | IllegalAccessException e) {
				throw new DataAccessException("Unable to create instance of foreign class.", e);
			}
		}
		
		relatedColumn.set(foreignRecord, value);
		
	}
	
	@Override
	public boolean isForeign() {
		return true;
	}
}
