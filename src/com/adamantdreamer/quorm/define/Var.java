package com.adamantdreamer.quorm.define;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotated string is set as a VARCHAR type. This has the same effect as <code>@Type(DataType.VARCHAR)</code>.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Var {
}
