package com.adamantdreamer.quorm.define;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Adds column to an index.
 * 
 * @param value Index name.  For the Primary Key, use {@link com.adamantdreamer.quorm.define.Id @Id} instead.
 * @param order Sorting order for each column in the index (first value = 0).<br>
 * &nbsp; The default value (-1) sorts the columns into build order.  
 * @param related <i>For foreign indices Only</i> Foreign column name. 
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Index {
	 public String value();
	 public int order() default -1;
	 public String related() default "";
}
