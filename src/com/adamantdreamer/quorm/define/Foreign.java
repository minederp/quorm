package com.adamantdreamer.quorm.define;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.adamantdreamer.quorm.elements.ReferenceOption;

/**
 * Creates a relationship to another table, generating field(s) based on the table's primary keys.<br><br>
 * <i>Example:</i><br>
 * <code>
 * public class Person {<br>
 * &nbsp; &#064;Id int id;<br>
 * &nbsp; String name;<br>
 * }<br>
 * public class Address {<br>
 * &nbsp; &#064;Foreign(onUpdate=ReferenceOption.CASCADE) Person per;<br>
 * }</code><br>
 * &nbsp; <i>creates an <code>per</code> field in the Address table, referencing <code>Person.id</code>.  If the person record is deleted, so is the original </i><br>
 * Two columns are created in the Address table, per_id1 and per_id2, referencing a record in Person class. If the related class only had one id.</i>
 * 
 * @param onUpdate Action to take when the related record is updated.   
 * @param onDelete Action to take when the related record record is deleted.
 * <br><br>   

 * </code>
*/
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Foreign {
	public ReferenceOption onUpdate() default ReferenceOption.DEFAULT;
	public ReferenceOption onDelete() default ReferenceOption.DEFAULT;
}
