package com.adamantdreamer.quorm.define;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>Changes the default value for the column.  This will *not* update a new instance of object.<br>
 *   
 * <p>For a Timestamp, the following values can also be used:<br>
 * &nbsp; <b>Default.CREATE_TIMESTAMP</b> will have the current date/time used when a record is created.<br>
 * &nbsp; <b>Default.UPDATE_TIMESTAMP</b> will also update the field's date/time when the record is updated.<br>
 * <code>create()</code> and <code>update()</code> DAO functions will update the Timestamp object. 
 * </p>  
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Default {
	final static public String CREATE_TIMESTAMP = "CURRENT_TIMESTAMP";
	final static public String UPDATE_TIMESTAMP = "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP";
	
	public String value();
}
