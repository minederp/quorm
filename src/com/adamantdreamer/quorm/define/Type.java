package com.adamantdreamer.quorm.define;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.adamantdreamer.quorm.elements.DataType;

/**
 *  Set a column's {@link com.adamantdreamer.quorm.elements.DataType DataType}.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Type {
	public DataType value() default DataType.BLOB;
}
