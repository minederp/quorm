package com.adamantdreamer.quorm.define;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.adamantdreamer.quorm.elements.ReferenceOption;
import com.adamantdreamer.quorm.index.IndexType;

/**
 * @param value Index name.
 * @param type {@link com.adamantdreamer.quorm.index.IndexType IndexType} to use. Default is INDEX.<br>
 * <i>For foreign keys only:</i>  
 * @param Reference database.table_name Example: <code>Accounts.Person</code>
 * @param onUpdate How to handle when a related record is updated on the foreign table.   
 * @param onDelete How to handle when a related record is removed from the foreign table.   
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface DefineIndex {
	public String value();
	public IndexType type() default IndexType.INDEX;
	public String table() default "";
	public ReferenceOption onUpdate() default ReferenceOption.DEFAULT;
	public ReferenceOption onDelete() default ReferenceOption.DEFAULT;	
}
