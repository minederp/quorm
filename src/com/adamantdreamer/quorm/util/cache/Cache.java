package com.adamantdreamer.quorm.util.cache;

import java.util.Map;
import java.util.Set;

public interface Cache<K,V> {
	
	public V put(K key, V value);
	public V get(Object key);
	public V remove(Object key);
	public boolean containsKey(Object key);

	public void putAll(Map<? extends K, ? extends V> m);
	public void clear();
	public void clearQueue();
	public Set<K> keySet();
}
