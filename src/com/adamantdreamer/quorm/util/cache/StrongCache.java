package com.adamantdreamer.quorm.util.cache;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.WeakHashMap;

public class StrongCache<K,V> implements Cache<K,V> { 

	private Map<K,V> map = new WeakHashMap<>();
	//private ReferenceQueue<V> queue = new ReferenceQueue<>();

	public void clearQueue() {
		// No Queres to clear in a Strong Cache.
	}
	
	public V put(K key, V value) {
		return map.put(key, value);
	}

	public boolean containsKey(Object key) {
		return map.containsKey(key);
	}

	public V get(Object key) {
		return map.get(key);
	}

	public V remove(Object key) {
		return map.remove(key);
	}

	public void putAll(Map<? extends K, ? extends V> m) {
		for(Entry<? extends K, ? extends V> entry: m.entrySet()) {
			put(entry.getKey(),entry.getValue());
		}
	}

	public void clear() {
		map.clear();
	}

	public Set<K> keySet() {
		return map.keySet();
	}
	
}
