package com.adamantdreamer.quorm.util.cache;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.WeakHashMap;

public class WeakCache<K,V> implements Cache<K,V> {
	
	private Map<K,WeakReference<V>> map = new WeakHashMap<>();
	private ReferenceQueue<V> queue = new ReferenceQueue<>();

	public void clearQueue() {
		Reference<? extends V> poll = null;
	    while ((poll = queue.poll()) != null) {
	    	map.values().remove(poll);
	    }
	}
	
	public V put(K key, V value) {
		clearQueue();
		Reference<V> oldValue = map.put(key, new WeakReference<V>(value, queue));
		if (oldValue == null) return null;
		return oldValue.get();
	}

	public boolean containsKey(Object key) {
		clearQueue();
		return map.containsKey(key);
	}

	public V get(Object key) {
		clearQueue();
		WeakReference<V> value = map.get(key);
		if (value == null) return null;
		return value.get();
	}

	public V remove(Object key) {
		clearQueue();
		WeakReference<V> value = map.remove(key);
		if (value == null) return null;
		return value.get();
	}

	public void putAll(Map<? extends K, ? extends V> m) {
		clearQueue();
		for(Entry<? extends K, ? extends V> entry: m.entrySet()) {
			put(entry.getKey(),entry.getValue());
		}
	}

	public void clear() {
		clearQueue();
		map.clear();
	}

	public Set<K> keySet() {
		clearQueue();
		return map.keySet();
	}

}
