package com.adamantdreamer.quorm.util.patternbuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Generates a string based on multiple 
 * {@link com.adamantdreamer.quorm.util.patternbuilder.StringPattern StringPattern} layers. 
 */
public class PatternBuilder {

	// Used to determine if a separator is needed with append. 
	private boolean isFirst = true;
	
	private StringBuilder stringBuilder;
	private List<StringPattern> patterns = new ArrayList<>();

	/**
	 * Generates a new PatternBuilder using an internal StringBuilder.
	 *
	 * @param basePattern Starting pattern.
	 */
	public PatternBuilder(StringPattern basePattern) {
		this.stringBuilder = new StringBuilder();
		add(basePattern);		
	}
	
	/**
	 * Generates a new PatternBuilder using an external (pre-existing) StringBuilder.
	 *   
	 * @param stringBuilder StringBuilder to append to.
	 * @param basePattern   Starting pattern.
	 */
	public PatternBuilder(StringBuilder stringBuilder, StringPattern basePattern) {
		this.stringBuilder = stringBuilder;
		add(basePattern);		
	}

	/**
	 * Internal process, adds a single line using the current pattern.   
	 */
	private void appendLine(String content) {
		if (getCurrentPattern() == null) {
			return;
		}
		StringPattern pattern = getCurrentPattern();
		if (isFirst) {
			isFirst = false;
		} else {
			stringBuilder.append(pattern.getSeparator());
		}
		stringBuilder.append(content);
	}

	/**
	 * Appends one or more line(s), using the separator between each.
	 * 
	 * @param contents String or String array to append.  
	 * @return
	 */
	public PatternBuilder append(String... contents) {
		for (String line: contents) {
			appendLine(line);
		}
		return this;
	}
	
	/**
	 * Appends a String or array using a sub-pattern with contents, returning to the original 
	 * pattern once done. 
	 * 
	 * @param pattern  Pattern to use for the contents.
	 * @param contents String or String array to append.  
	 * @return
	 */
	public PatternBuilder append(StringPattern pattern, String... contents) {
		/* 
		 * This makes sure the separator of the old pattern is used before introducing the new pattern.
		 */
		appendLine("");
		pattern.append(stringBuilder, contents);
		return this;
	}	

	/**
	 * Selects a sub-pattern to use, allowing for one pattern to be used within another.  All
	 * append() statements will use this sub-pattern until {@link #end()} or {@link #toString()} 
	 * is called. Any number of layers can be used. 
	 *   
	 * @param pattern
	 * @return this
	 */
	public PatternBuilder add(StringPattern pattern) {
		/* 
		 * This makes sure the separator of the old pattern is used before introducing the new pattern.
		 */
		appendLine("");
		patterns.add(pattern);
		stringBuilder.append(pattern.getPrefix());
		isFirst = true;
		return this;
	}
	
	/**
	 * Ends the current pattern layer, adding the current pattern's suffix and 
	 * returning to the prior layer.
	 * 
	 * @return this
	 */
	public PatternBuilder end() {
		stringBuilder.append(getCurrentPattern().getSuffix());
		patterns.remove(patterns.size() - 1);
		return this;
	}
	
	/**
	 * Ends all pattern layers, adding each suffix to the end.
	 * 
	 * @return
	 */
	public PatternBuilder endAll() {
		while (patterns.size() > 0) {
			end();
		}
		return this;
	}

	/**
	 * Removes all patterns and starts a new StringBuilder. 
	 */
	public void clear() {
		patterns.clear();
		stringBuilder = new StringBuilder();
	}
	
	/**
	 * Retrieves the StringBuilder for external checking and/or editing. 
	 * <br><b>Note:</b> Calling the StringBuilders toString() function will not
	 * generate the suffixes for the current pattern layer(s), use PatternBuilder's 
	 * endAll() or toString() to do so.
	 *    
	 * @return StringBuilder
	 */
	public StringBuilder getString() {
		return stringBuilder;
	}


	/**
	 * Returns the current StringPattern, or null if none.
	 */
	public StringPattern getCurrentPattern() {
		if (patterns.size() == 0) {
			return null;
		} else {
			return patterns.get(patterns.size() - 1);
		}
	}
	
	/**
	 * Returns an immutable list, representing the current StringPatterns.
	 */
	public List<StringPattern> getPatterns() {
		return Collections.unmodifiableList(patterns);
	}	
	
	/**
	 * Ends all current Patterns, and converts the PatternBuilder to a String.
	 * If the current state is needed without ending
	 * 
	 * @return String
	 */
	public String toString() {
		endAll();
		return stringBuilder.toString();
	}

	
}
