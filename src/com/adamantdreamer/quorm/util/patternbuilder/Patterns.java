package com.adamantdreamer.quorm.util.patternbuilder;

/**
 * Common {@link StringPattern}s used in SQL query's.
 */
public class Patterns {

	/**
	 * Prefix: None<br>
	 * Separator: Space<br>
	 * Suffix: None
	 */
	public static final StringPattern SPACE = new StringPattern(" ");
	/**
	 * Prefix: None<br>
	 * Separator: ,<br>
	 * Suffix: None
	 */
	public static final StringPattern COMMA = new StringPattern(",");
	/**
	 * Prefix: None<br>
	 * Separator: .<br>
	 * Suffix: None
	 */
	public static final StringPattern DOT = new StringPattern(".");
	/**
	 * Prefix: (<br>
	 * Separator: ,<br>
	 * Suffix: )
	 */
	public static final StringPattern VALUE = new StringPattern("(", ",", ")");
	/**
	 * Prefix: None<br>
	 * Separator: =?,<br>
	 * Suffix: =?
	 */
	public static final StringPattern UPDATE = new StringPattern("", "=?,", "=?");
	/**
	 * Prefix: None<br>
	 * Separator: "=? AND "<br>
	 * Suffix: "=?"
	 */
	public static final StringPattern KEY = new StringPattern("", "=? AND ", "=?");
	/**
	 * Prefix: None<br>
	 * Separator: " AND "<br>
	 * Suffix: None
	 */
	public static final StringPattern AND = new StringPattern(" AND ");

}
