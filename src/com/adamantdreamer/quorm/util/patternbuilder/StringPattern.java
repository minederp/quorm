package com.adamantdreamer.quorm.util.patternbuilder;

/**
 * Creates a pattern to easily append String arrays to 
 * {@link java.lang.StringBuilder StringBuilder}'s, allowing for prefix, suffix, and 
 * separator strings.
 */
public class StringPattern {
	public String prefix, separator, suffix;

	/**
	 * Creates a pattern to easily append {@link java.lang.StringBuilder StringBuilder}'s.
	 * 
	 * @param prefix   Added before the first value. 
	 * @param separator Added in between each value of the array. If the array is one item long,
	 * 					no separator will be used.
	 * @param suffix Added after the last value.
	 */
	public StringPattern(String prefix, String separator, String suffix) {
		this.prefix = prefix;
		this.separator = separator;
		this.suffix = suffix;
	}

	/**
	 * Creates a pattern to easily append {@link java.lang.StringBuilder StringBuilder}'s.
	 * 
	 * @param separator Added in between each value of the array.
	 */
	public StringPattern(String separator) {
		this.prefix = "";
		this.separator = separator;
		this.suffix = "";
	}

	public String getPrefix() {
		return prefix;
	}

	public String getSeparator() {
		return separator;
	}

	public String getSuffix() {
		return suffix;
	}

	/**
	 * Appends a StringBuilder with the <b>prefix+content+suffix</b>.
	 * 
	 * @param stringBuilder	Builder to append.
	 * @param content Content to add to the builder.
	 * @return this
	 */
	public StringPattern append(StringBuilder stringBuilder, String content) {
		stringBuilder.append(prefix).append(content).append(suffix);
		return this;
	}
	
	/**
	 * Appends a StringBuilder with the <b>prefix+contents+suffix</b>. In between 
	 * each value the <b>separator</b> will be added.
	 *  
	 * @param stringBuilder	Builder to append.
	 * @param contents String array containing the values to add.
	 * @return this
	 */
	public StringPattern append(StringBuilder stringBuilder, String... contents) {
		stringBuilder.append(prefix);
		stringBuilder.append(contents[0]);
		for (int i = 1; i < contents.length; i++) {
			stringBuilder.append(separator).append(contents[i]);
		}
		stringBuilder.append(suffix);
		return this;
	}

	/**
	 * Returns a combined string based on a list of contents.
	 */
	public String apply(String... contents) {
		return append(new StringBuilder(), contents).toString();
	}
	
}
