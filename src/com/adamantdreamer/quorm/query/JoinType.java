package com.adamantdreamer.quorm.query;

public enum JoinType {
	LEFT, LEFT_OUTER, OUTER, RIGHT, RIGHT_OUTER, THETA, INNER;
	
	public String toString() {
		return name().replace("_", " ");
	}
}
