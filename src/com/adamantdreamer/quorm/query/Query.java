package com.adamantdreamer.quorm.query;

import java.sql.SQLException;

public interface Query<T> {
	
	public Query<T> where(String criteria);
	
	public Query<T> as(String tableName);
	
	public Query<T> join(JoinType join, Class<?> table, String on);
	
	public Query<T> values(String... values);
	
	public void append(Class<?> table);
	public void append(String text);
	
	public Query<T> orderBy(String... values);

	public String getString();
	
	public int update() throws SQLException;
	public int update(Object... values) throws SQLException;
	public QueryResult<T> read() throws SQLException;
	public QueryResult<T> read(Object... values) throws SQLException;	
	public PreparedQuery<T> prepare() throws SQLException;

}
