package com.adamantdreamer.quorm.query;

public enum QueryType {
	CREATE, READ, UPDATE, DELETE, KEY, TIMESTAMP;
}
