package com.adamantdreamer.quorm.query;

import java.io.Closeable;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.adamantdreamer.quorm.core.TableDAO;

public class QueryResult<T> implements Closeable {
	
	//Class<T> table;
	PreparedQuery<T> preparedQuery;
	TableDAO<T> tableDao;
	ResultSet resultSet;

	public QueryResult(PreparedQuery<T> preparedQuery, ResultSet resultSet) {
		//this.table = table;
		this.tableDao = preparedQuery.getDAO().get(preparedQuery.getTable());
		this.preparedQuery = preparedQuery;
		this.resultSet = resultSet;
	}
	
	public ResultSet getData() {
		return resultSet;
	}
	
	public T first() throws SQLException {
		if (resultSet.isBeforeFirst()) {
			resultSet.next();
			return tableDao.read(resultSet);
		} else {
			return null;
		}
	}
	
	public boolean isBeforeFirst() throws SQLException {
		return resultSet.isBeforeFirst();
	}
	
	public boolean next() throws SQLException {
		return resultSet.next();
	}
	
	public T read() throws SQLException {
		return tableDao.read(resultSet);
	}
	
	public List<T> readAll() throws SQLException {
		if (!resultSet.isBeforeFirst()) {
			return null;
		}
		List<T> results = new ArrayList<T>();
		while (resultSet.next()) {
			results.add(tableDao.read(resultSet));
		}
		return results;
	}

	@Override
	public void close() throws IOException {
		preparedQuery.close();
	}
	
}
