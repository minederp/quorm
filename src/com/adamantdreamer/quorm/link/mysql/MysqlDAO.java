package com.adamantdreamer.quorm.link.mysql;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.adamantdreamer.quorm.core.DAO;
import com.adamantdreamer.quorm.core.Link;
import com.adamantdreamer.quorm.core.Schema;
import com.adamantdreamer.quorm.core.TableDAO;
import com.adamantdreamer.quorm.elements.TableManager;
import com.adamantdreamer.quorm.exception.ColumnMismatchException;
import com.adamantdreamer.quorm.query.Query;
import com.adamantdreamer.quorm.query.QueryType;

class MysqlDAO implements DAO {
	
	MysqlLink link;
	Map<Class<?>, MysqlTableDAO<?>> tableDAOMap = new HashMap<>();
	
	public MysqlDAO(MysqlLink link) {
		this.link = link;
	}
	
	@Override
	public Link getLink() {
		return this.link;
	}

// Table DAO //
	
	@SuppressWarnings("unchecked")
	public <T> TableDAO<T> add(Class<T> table) {
		if (tableDAOMap.containsKey(table)) {
			return (TableDAO<T>)tableDAOMap.get(table);
		} else {
			MysqlTableDAO<T> tableDAO = new MysqlTableDAO<T>(link, TableManager.get(table)); 
			tableDAOMap.put(table, tableDAO);
			return tableDAO;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> TableDAO<T> get(Class<T> table) {
		return (TableDAO<T>)tableDAOMap.get(table);
	}
	
	@Override
	public void build(Schema... schemas) throws SQLException, ColumnMismatchException {
		MysqlBuilder builder = new MysqlBuilder(link);
		for (Schema schema: schemas) {
			builder.build(schema);
			for (Class<?> table: schema.getTables()) {
				add(table);
			}
		}
	}	

	@Override
	public void build(Class<?>... tables) throws SQLException, ColumnMismatchException {
		Set<Schema> schemas = new HashSet<>();
		for (Class<?> table: tables) {
			if (TableManager.get(table) == null) {
				schemas.add(TableManager.add(table));
				add(table);
			}
		}

		MysqlBuilder builder = new MysqlBuilder(link);
		for (Schema schema: schemas) {
			builder.build(schema);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> T create(T record) throws SQLException { 
		return ((TableDAO<T>) tableDAOMap.get(record.getClass())).create(record);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T read(T record) throws SQLException { 
		return ((TableDAO<T>) tableDAOMap.get(record.getClass())).read(record);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> boolean update(T record) throws SQLException { 
		return ((TableDAO<T>) tableDAOMap.get(record.getClass())).update(record);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> boolean delete(T record) throws SQLException { 
		return ((TableDAO<T>) tableDAOMap.get(record.getClass())).delete(record);
	}
	
// Query //

	@Override
	public <T> Query<T> create(Class<T> table) {
		return new MysqlQuery<T>(QueryType.CREATE, this, table);
	}

	@Override
	public <T> Query<T> read(Class<T> table) {
		return new MysqlQuery<T>(QueryType.READ, this, table);
	}

	@Override
	public <T> Query<T> update(Class<T> table) {
		return new MysqlQuery<T>(QueryType.UPDATE, this, table);
	}

	@Override
	public <T> Query<T> delete(Class<T> table) {
		return new MysqlQuery<T>(QueryType.DELETE, this, table);
	}

	@Override
	public <T> Query<T> query(String sqlString) {
		return new MysqlQuery<T>(this, sqlString);
	}
	

}
