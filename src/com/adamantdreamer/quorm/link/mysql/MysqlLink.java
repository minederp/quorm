package com.adamantdreamer.quorm.link.mysql;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import com.adamantdreamer.quorm.core.Link;

public class MysqlLink implements Link {
	
	private String host, port, username, password;
	
	private String MySQLURL;
	private Connection connection;
	@SuppressWarnings("unused")
	private Driver driver;
	private Statement statement;
	private MysqlDAO dao;
	
	public MysqlLink(String host, String port, String username, String password) {
		super();
		this.host = host;
		this.port = port;
		this.username = username;
		this.password = password;
		this.dao = new MysqlDAO(this);
	}
	
	public void setLogin(String host, String port, String username, String password) {
		this.host = host;
		this.port = port;
		this.username = username;
		this.password = password;
	}
	
	public void connect() throws ClassNotFoundException, SQLException {
		MySQLURL = "jdbc:mysql://" + host + ":" + port + "/";
		Class.forName("com.mysql.jdbc.Driver");
		connection = DriverManager.getConnection(MySQLURL, username, password);
		connection.setAutoCommit(true);
		statement = connection.createStatement();
	}
	
	public Connection getConnection() {
		return connection;
	}
	
	public Statement getStatement() {
		return statement;
	}

	@Override
	public MysqlDAO getDAO() {
		return dao;
	}

	@Override
	public void close() {
		try {
			connection.close();
		} catch (SQLException e) {
			// Connection is already closed, ignore.
		}
	}

	
}
