package com.adamantdreamer.quorm.link.mysql;

import java.util.Collection;
import java.util.Map;

import com.adamantdreamer.quorm.elements.Column;
import com.adamantdreamer.quorm.elements.DataType;
import com.adamantdreamer.quorm.elements.TableStructure;
import com.adamantdreamer.quorm.query.QueryType;
import com.adamantdreamer.quorm.util.patternbuilder.PatternBuilder;
import com.adamantdreamer.quorm.util.patternbuilder.Patterns;

class MysqlQueryBuilder {
	
//	private TableStructure<?> table;
	
//	private String insert, select, update, delete, key, timestamp;
	
	static public void build(TableStructure<?> table, Map<QueryType, String> queries) {
		queries.put(QueryType.CREATE, buildCreate(table));
		queries.put(QueryType.READ, buildRead(table));
		queries.put(QueryType.UPDATE, buildUpdate(table));
		queries.put(QueryType.DELETE, buildDelete(table));
		queries.put(QueryType.KEY, buildKey(table));
		queries.put(QueryType.TIMESTAMP, buildTimestamps(table));
	}

	static private String buildCreate(TableStructure<?> table) {
		PatternBuilder query = new PatternBuilder(Patterns.SPACE);
		PatternBuilder values = new PatternBuilder(Patterns.VALUE);
		
		query.append("INSERT INTO");
		query.append(Patterns.DOT, table.getSchema().getName(), table.getName());
		
		query.add(Patterns.VALUE);
		for (Column column: table.getColumns()) {
			if (!column.isAutoIncrement() 
					&& (column.getType() != DataType.TIMESTAMP)) {
				query.append(Patterns.DOT, column.getName());
				values.append("?");
			}
		}
		query.end();

		query.append("VALUES").append(values.toString());
		
		return query.toString();
	}
	
	static private String buildRead(TableStructure<?> table) {
		PatternBuilder builder = new PatternBuilder(Patterns.SPACE);
		builder.append("SELECT");
		
		builder.add(Patterns.COMMA);
		for (Column column: table.getColumns()) {
			builder.append(Patterns.DOT, column.getTable().getName(), column.getName());
		}
		builder.end();
		
		builder.append("FROM")
				.append(Patterns.DOT,table.getSchema().getName(),table.getName());
		return builder.toString();		
	}
	
	static private String buildUpdate(TableStructure<?> table) {
		PatternBuilder builder = new PatternBuilder(Patterns.SPACE);
		builder.append("UPDATE")
			.append(Patterns.DOT, table.getSchema().getName(),table.getName())
			.append("SET");

		Collection<Column> setColumns = table.getColumns();
		for(Column column: table.getPrimaryKey().getColumns()) {
			setColumns.remove(column);
		}
	
		builder.add(Patterns.UPDATE);
		for (Column column: setColumns) {
			builder.append(Patterns.DOT, column.getName());
		}
		builder.end();
		
		return builder.toString();

	}

	static private String buildDelete(TableStructure<?> table) {
		PatternBuilder builder = new PatternBuilder(Patterns.SPACE);
		builder.append("DELETE FROM")
			.append(Patterns.DOT, table.getSchema().getName(),table.getName());
		return builder.toString();
	}
	
	static private String buildKey(TableStructure<?> table) {
		PatternBuilder query = new PatternBuilder(Patterns.KEY);
		
		for (Column column: table.getPrimaryKey().getColumns()) {
			query.append(column.getName());
		}
		
		return query.toString();
	}

	static private String buildTimestamps(TableStructure<?> table) {
		boolean hasTimestamp = false;
		PatternBuilder builder = null;
		
		for (Column column: table.getColumns()) {
			if (column.getType() == DataType.TIMESTAMP) {
				if (!hasTimestamp) {
					hasTimestamp = true;
					builder = new PatternBuilder(Patterns.SPACE);
					builder.append("SELECT");
					builder.add(Patterns.COMMA);					
				}
				builder.append(Patterns.DOT, column.getTable().getName(), column.getName());
			}
		}
		
		// No time stamp found, return empty.
		if (!hasTimestamp) {
			return null;
		}
		
		// Ends comma pattern
		builder.end();
		builder.append("FROM")
				.append(Patterns.DOT,table.getSchema().getName(),table.getName());
		return builder.toString();	
	}

	
}
