package com.adamantdreamer.quorm.link.mysql;
import java.sql.SQLException;

import com.adamantdreamer.quorm.elements.TableManager;
import com.adamantdreamer.quorm.elements.TableStructure;
import com.adamantdreamer.quorm.query.JoinType;
import com.adamantdreamer.quorm.query.PreparedQuery;
import com.adamantdreamer.quorm.query.Query;
import com.adamantdreamer.quorm.query.QueryResult;
import com.adamantdreamer.quorm.query.QueryType;
import com.adamantdreamer.quorm.util.patternbuilder.PatternBuilder;
import com.adamantdreamer.quorm.util.patternbuilder.Patterns;

public class MysqlQuery<T> implements Query<T> {
	private Class<T> table;
	private MysqlDAO dao;
	private PatternBuilder sqlString;

	public MysqlQuery(QueryType type, MysqlDAO dao, Class<T> table) {
		this.dao = dao;
		this.table = table;
		sqlString = new PatternBuilder(Patterns.SPACE);
		sqlString.append(dao.get(table).getQueries().get(type));
	}

	public MysqlQuery(MysqlDAO dao, String sqlString) {
		this.dao = dao;
		this.sqlString = new PatternBuilder(Patterns.SPACE);
		this.sqlString.append(sqlString);
	}	


	public void append(Class<?> table) {
		TableStructure<?> data =  TableManager.get(table);
		//lastTable = table;
		sqlString.append(Patterns.DOT, data.getSchema().getName(), data.getName());
	}
	
	public void append(String text) {
		sqlString.append(text);
	}
	
	public MysqlQuery<T> as(String tableName) {
		sqlString.append("AS", tableName);
		return this;
	}
	
	public MysqlQuery<T> join(JoinType join, Class<?> table, String on) {
		sqlString.append(join.toString());
		append(table);
		sqlString.append("ON", on);
		return this;
	}
	
	public MysqlQuery<T> values(String... values) {
		sqlString.append(Patterns.COMMA, values);
		return this;
	}
	
	public MysqlQuery<T> where(String criteria) {
		if (sqlString.getString().substring(0, 6).equalsIgnoreCase("INSERT")) {
			throw new IllegalStateException("WHERE is not a valid function for insert statements.");
		}
		if (sqlString.getCurrentPattern() != Patterns.AND) {
			sqlString.append("WHERE");
			sqlString.add(Patterns.AND);
		}
		sqlString.append(criteria);	
		return this;
	}
	
	public MysqlQuery<T> and(String criteria) {
		where(criteria);
		return this;
	}	
	
	public MysqlQuery<T> orderBy(String... values) {
		sqlString.append("ORDER BY");
		sqlString.append(Patterns.COMMA, values);
		return this;
	}
	
	public String getString() {
		sqlString.endAll();
		return sqlString.toString();
	}

	
	public PreparedQuery<T> prepare() throws SQLException {
		return new PreparedQuery<T>(table, dao, getString());
	}

	public int update() throws SQLException {
		try (PreparedQuery<T> prep = new PreparedQuery<T>(table, dao, getString())) {
			return prep.update();
		}
	}
	
	public int update(Object... values) throws SQLException {
		try (PreparedQuery<T> prep = new PreparedQuery<T>(table, dao, getString())) {
			prep.setAll(values);
			return prep.update();
		}
	}	
	
	public QueryResult<T> read() throws SQLException {
		/* Suppressed per object needs to stay open until the processing
		 * is done for the QueryResult.  QueryResult's closure method will
		 * also close the parent PreparedQuery object. 
		 */
		@SuppressWarnings("resource")
		PreparedQuery<T> prep = new PreparedQuery<T>(table, dao, getString());
		return prep.read();
	}

	public QueryResult<T> read(Object... values) throws SQLException {
		/* Suppressed per object needs to stay open until the processing
		 * is done for the QueryResult.  QueryResult's closure method will
		 * also close the parent PreparedQuery object. 
		 */
		@SuppressWarnings("resource")
		PreparedQuery<T> prep = new PreparedQuery<T>(table, dao, getString());
		prep.setAll(values);
		return prep.read();
	}


	
}
