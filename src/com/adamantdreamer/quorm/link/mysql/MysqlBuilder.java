package com.adamantdreamer.quorm.link.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.adamantdreamer.quorm.core.Schema;
import com.adamantdreamer.quorm.define.NoBuild;
import com.adamantdreamer.quorm.elements.Column;
import com.adamantdreamer.quorm.elements.DataType;
import com.adamantdreamer.quorm.elements.TableManager;
import com.adamantdreamer.quorm.elements.TableStructure;
import com.adamantdreamer.quorm.exception.ColumnMismatchException;
import com.adamantdreamer.quorm.index.BaseIndex;
import com.adamantdreamer.quorm.index.ForeignIndex;
import com.adamantdreamer.quorm.index.IndexType;
import com.adamantdreamer.quorm.util.patternbuilder.PatternBuilder;
import com.adamantdreamer.quorm.util.patternbuilder.Patterns;

/**
 * Handles creation and verification of schemas/tables.
 */
class MysqlBuilder {
	private MysqlLink link;
	
	public MysqlBuilder(MysqlLink link) {
		this.link = link;
	}
	
	public void build(Schema schema) throws SQLException, ColumnMismatchException {
		link.getStatement().executeUpdate("CREATE SCHEMA IF NOT EXISTS " + schema.getName());
		
		Set<String> currentTables = getTableNames(schema);
				
		for (Class<?> table: schema.getTables()) {
			TableStructure<?> data = TableManager.get(table);
			if (currentTables.contains(data.getName())) {
				check(table);
			} else if (table.getClass().getAnnotation(NoBuild.class) == null) {
				build(table);
			}
		}
				
	}
	
	/**
	 * Returns a Case Insensitive Set of Table Names contained in a Schema.
	 */
	private Set<String> getTableNames(Schema schema) throws SQLException {
		Set<String> result = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
		try (ResultSet rs = link.getStatement().executeQuery("SHOW TABLES FROM " + schema.getName())) {
			if (rs.isBeforeFirst()) {
				while (rs.next()) {
					result.add(rs.getString(1));
				}
			}
		} 
		return result;
	}	
	
	private void build(Class<?> table) throws SQLException {
		link.getDAO().add(table);
		TableStructure<?> data = TableManager.get(table);
		
		PatternBuilder sqlStr = new PatternBuilder(Patterns.SPACE);
		
		sqlStr.append("CREATE TABLE");
		sqlStr.append(Patterns.DOT, data.getSchema().getName(), data.getName());
		
		sqlStr.add(Patterns.VALUE);
		
		// Build Columns
		for (Column column: data.getColumns()) {
			addColumnToBuilder(sqlStr, column);
		}
		
		// Build Indices
		for (BaseIndex index: data.getIndices()) {
			if (index.getColumns().size() == 0) {
				continue;
			}
			
			sqlStr.add(Patterns.SPACE);
			
			switch (index.getType()) {
			case PRIMARY_KEY:
				sqlStr.append("PRIMARY KEY");
				break;
			case UNIQUE_KEY:
				sqlStr.append("UNIQUE KEY");
				break;
			case FOREIGN_KEY:
				sqlStr.append("FOREIGN KEY");
				break;
			case INDEX:
				sqlStr.append("INDEX");
			default:
				break;
			}
			
			if (index.getType() != IndexType.PRIMARY_KEY) {
				sqlStr.append(index.getName());
			}
			
			sqlStr.add(Patterns.VALUE);
			for (Column column: index.getColumns()) {
				sqlStr.append(column.getName());
			}
			sqlStr.end();
			
			if (index.getType() == IndexType.FOREIGN_KEY) {
				ForeignIndex foreign = (ForeignIndex)index;
				sqlStr.append("REFERENCES",foreign.getForeignTable());
				sqlStr.add(Patterns.VALUE);
				for (String reference: foreign.getForeignReferences()) {
					sqlStr.append(reference);
				}
				sqlStr.end();
				sqlStr.append("ON UPDATE",foreign.onUpdate().getString(),
						"ON DELETE",foreign.onDelete().getString());
			}
			
			sqlStr.end();
		}

		System.out.println(sqlStr.toString());
		link.getStatement().executeUpdate(sqlStr.toString());
	}

	private void check(Class<?> table) throws SQLException, ColumnMismatchException {
		link.getDAO().add(table);
		
		TableStructure<?> data = TableManager.get(table);
		
		// Compare Object Columns to Database Columns
		List<Column> dbColumns = getDatabaseColumns(data);
		Collection<Column> tableColumns = data.getColumns();
		
		for (Column tableColumn : tableColumns) {
			Column dbColumn = findMatch(dbColumns, tableColumn);
			if (dbColumns != null) {
				dbColumns.remove(dbColumn);
				if (! tableColumn.equals(dbColumn)) {
					throw new ColumnMismatchException(dbColumn, tableColumn);
				}
			} else {
				// adds any missing columns.
				PatternBuilder query = new PatternBuilder(Patterns.SPACE);
				query.append("ALTER_TABLE");
				query.append(Patterns.DOT, data.getSchema().getName(), data.getName());
				query.append("ADD");
				addColumnToBuilder(query, dbColumn);
				link.getStatement().executeUpdate(query.toString());
			}
		} 
		
	}
	
	private Column findMatch(List<Column> columns, Column column) {
		for (Column fieldSearch: columns) {
			if (column.getName().equalsIgnoreCase(fieldSearch.getName())) {
				return column;
			}
		}
		return null;
	}

	private void addColumnToBuilder(PatternBuilder query, Column column) {
		// Add Name
		query.add(Patterns.SPACE);
		query.append(Patterns.DOT, column.getName());
		
		// Add Type 
		query.append(column.getType().toString());
		if (column.getSize() > 0) {
			query.append(Patterns.VALUE, Integer.toString(column.getSize()));
		}
		
		// Additional Arguments
		if (column.isAutoIncrement()) {
			query.append("AUTO_INCREMENT");
		}
		
		if (column.isNotNull()) {
			query.append("NOT NULL");
		}
		
		if (column.getDefaultValue() != null) {
			query.append("DEFAULT").append(column.getDefaultValue());
		}
		query.end();
	}
	
	private List<Column> getDatabaseColumns(TableStructure<?> table) throws SQLException {
		List<Column> columns= new ArrayList<Column>();
		// Check Columns
		try (ResultSet rs = link.getStatement().executeQuery(
				"SELECT column_name, data_type, character_maximum_length, is_nullable, column_default, extra "
				+ "FROM INFORMATION_SCHEMA.columns where table_schema='" + table.getSchema().getName() 
				+ "' and table_name='" + table.getName() + "'")) {
			if (rs.isBeforeFirst()) {
				while (rs.next()) {
					columns.add(new Column(table, null,  
							rs.getString(1), DataType.get(rs.getString(2)), rs.getInt(3), 
							rs.getBoolean(4), rs.getString(5), rs.getString(6).contains("NOT NULL")));
				}
			}
		}
		return columns;
	}
	

}
