package com.adamantdreamer.quorm.link.sqlite;

import java.sql.Connection;
import java.sql.Statement;

import com.adamantdreamer.quorm.core.DAO;
import com.adamantdreamer.quorm.core.Link;

public class SqliteLink implements Link {

	@Override
	public void connect() throws Exception {
	}

	@Override
	public void close() {
	}

	@Override
	public Connection getConnection() {
		return null;
	}

	@Override
	public Statement getStatement() {
		return null;
	}

	@Override
	public DAO getDAO() {
		return null;
	}

}
