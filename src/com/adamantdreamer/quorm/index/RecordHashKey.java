package com.adamantdreamer.quorm.index;

import com.adamantdreamer.quorm.elements.Column;

public class RecordHashKey {
	private BaseIndex indexStructure;
	private Object record;

	public RecordHashKey(BaseIndex indexStructure, Object record) {
		this.indexStructure = indexStructure;
		this.record = record;
	}
	
	public BaseIndex getKey() {
		return indexStructure;
	}
	
	public Object getRecord() {
		return record;
	}

	@Override
	public int hashCode() {
		int value = 0;
		for (Column columns: indexStructure.getColumns()) {
			value = value ^ columns.get(record).hashCode();
		}
		return value;
	}
	
	@Override
	public boolean equals(Object object) {
		if (!(object instanceof RecordHashKey)) return false;
		RecordHashKey objIndex = (RecordHashKey)object;
		if (indexStructure != objIndex.getKey()) return false;
		for (Column columns: indexStructure.getColumns()) {
			if (!(columns.get(record).equals(columns.get(objIndex.getRecord())))) {
				return false;
			}
		}
		return true;
	}
}
