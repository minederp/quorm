package com.adamantdreamer.quorm.index;

import java.util.ArrayList;
import java.util.List;

import com.adamantdreamer.quorm.elements.Column;

/**
 * Covers index, primary keys, and unique keys.
 *
 */
public abstract class BaseIndex {
	
	private List<Column> columns = new ArrayList<>();

	private IndexType type;
	private String name;

	protected void setType(IndexType type) {
		this.type = type;
	}
	
	protected void setName(String name) {
		this.name = name;
	}
	
	public IndexType getType() {
		return type;
	}

	public String getName() {
		return name;
	}
	
	public List<Column> getColumns() {
		return columns;
	}
	
	public RecordHashKey index(Object record) {
		return new RecordHashKey(this, record);
	}
	
}
