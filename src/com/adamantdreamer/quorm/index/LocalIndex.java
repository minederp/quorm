package com.adamantdreamer.quorm.index;

import com.adamantdreamer.quorm.elements.Column;

public class LocalIndex extends BaseIndex {
	
	public LocalIndex(String name, IndexType type) {
		if (type == IndexType.FOREIGN_KEY) {
			throw new IllegalArgumentException("IndexType.ForeignIndex requires ForeignIndex class.");
		}
		
		setName(name);
		setType(type);
	}
	
	public boolean addColumn(Column column) {
		return getColumns().add(column);
	}
	
	public void setColumn(int index, Column column) {
		if (index == -1) {
			addColumn(column);
			return;
		}
		while (getColumns().size() <= index) {
			getColumns().add(null);
		}
		getColumns().set(index, column);
	}
	
	
}
