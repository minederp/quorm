package com.adamantdreamer.quorm.index;

public enum IndexType {
	PRIMARY_KEY,
	UNIQUE_KEY,
	INDEX,
	FOREIGN_KEY;
}
