package com.adamantdreamer.quorm.index;

import java.util.ArrayList;
import java.util.List;

import com.adamantdreamer.quorm.elements.Column;
import com.adamantdreamer.quorm.elements.ReferenceOption;

public class ForeignIndex extends BaseIndex {

	private List<String> foreignReferences;
	private String foreignTable; 
	private ReferenceOption onUpdate;
	private ReferenceOption onDelete;
	
	public ForeignIndex(String name, String foreignTable, 
			ReferenceOption onUpdate, ReferenceOption onDelete) {
		setName(name);
		setType(IndexType.FOREIGN_KEY);
		this.foreignTable = foreignTable;
		this.foreignReferences = new ArrayList<String>();
		this.onUpdate = onUpdate;
		this.onDelete = onDelete;
	}

	public ReferenceOption onUpdate() {
		return onUpdate;
	}
	
	public ReferenceOption onDelete() {
		return onDelete;
	}	
	
	public String getForeignTable() {
		return foreignTable;
	}
	
	public void addColumn(Column column, String foreignReference) {
		getColumns().add(column);
		foreignReferences.add(foreignReference);
	}
	
	public void setColumn(int index, Column column, String foreignReference) {
		if (index == -1) {
			addColumn(column, foreignReference);
			return;
		}
		getColumns().set(index, column);
		while (foreignReferences.size() <= index) {
			foreignReferences.add(null);
		}
		foreignReferences.set(index, foreignReference);
	}
	
	public List<String> getForeignReferences() {
		return foreignReferences;
	}
	
	
	
}
